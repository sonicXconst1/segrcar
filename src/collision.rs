use crate::line::{LineBundle, Line, Point};

use bevy::render::color::Color;
use bevy::ecs::system::IntoSystem;
use bevy::math::{Vec2, Vec3, vec3};
use bevy::transform::components::Transform;

pub type Size = Vec2;

pub enum Collider {
    Quad(Size),
}

fn rect(transform: &Transform, size: &Vec2) -> (Vec3, Vec3) {
    let half = (size.extend(0f32) * transform.scale) / 2f32;
    (transform.translation - half, transform.translation + half)
}

pub fn quad_collision(
    first_transform: &Transform,
    first_size: Vec2,
    second_transform: &Transform,
    second_size: Vec2
) -> bool {
    let (first_left_bottom, first_right_top) = rect(
        first_transform, &first_size);
    let (second_left_bottom, second_right_top) = rect(
        second_transform, &second_size);
    first_right_top.x >= second_left_bottom.x
        && first_left_bottom.x <= second_right_top.x
        && first_left_bottom.y <= second_right_top.y
        && first_right_top.y >= second_left_bottom.y
}

pub struct DrawCollidersPlugin;

impl bevy::app::Plugin for DrawCollidersPlugin {
    fn build(&self, builder: &mut bevy::app::AppBuilder) {
        builder
            .add_startup_system(setup.system())
            .add_system(draw_colliders_system.system());
    }
}

pub struct ColliderMesh;

fn setup(
    mut commands: bevy::ecs::system::Commands,
) {
    commands
        .spawn_bundle(LineBundle::from_points(Vec::new(), Color::RED))
        .insert(ColliderMesh);
}

fn draw_colliders_system(
    colliders: bevy::ecs::system::Query<(&Collider, &Transform)>,
    mut collider_mesh_query: bevy::ecs::system::Query<
        &mut Line,
        bevy::ecs::query::With<ColliderMesh>
    >
) {
    let mut collider_mesh = collider_mesh_query.single_mut()
        .expect("More then one collider mesh.");
    collider_mesh.points.clear();
    for (collider, transform) in colliders.iter() {
        match collider {
            Collider::Quad(size) => add_quad(
                &mut collider_mesh.points,
                transform,
                size),
        }
    }
}

fn add_quad(points: &mut Vec<Point>, transform: &Transform, size: &Vec2) {
    let (left_bottom, right_top) = rect(transform, size);
    points.push(Point(left_bottom, vec3(left_bottom.x, right_top.y, right_top.z)));
    points.push(Point(vec3(left_bottom.x, right_top.y, right_top.z), right_top));
    points.push(Point(right_top, vec3(right_top.x, left_bottom.y, left_bottom.z)));
    points.push(Point(vec3(right_top.x, left_bottom.y, left_bottom.z), left_bottom));
}
