use crate::{
    {Road, RoadNormals, RoadWireframes, RoadPath},
    road::{generate_road, sections_into_line, RoadSection},
    line::{LineBundle, Line, Point, line_to_normals, line_to_points, mesh_to_points},
    road_builder::RoadPoint};
use bevy::{
    log::info,
    math::{vec2, Vec3, Size},
    ui::{Val, Style, JustifyContent, AlignItems, Interaction, widget::Button},
    transform::components::Transform,
    text::{Text, TextStyle},
    ecs::{
        world::{World, FromWorld},
        system::{IntoSystem, Res, ResMut, Query, QuerySingleError},
    },
    transform::hierarchy::BuildChildren,
    ecs::query::{With, Without, Changed},
    asset::{Handle, Assets, AssetServer},
    sprite::ColorMaterial,
    render::{mesh::Mesh, color::Color},
    ui::entity::{UiCameraBundle, ButtonBundle, TextBundle},
    sprite::{Sprite, entity::SpriteBundle},
};

pub struct ButtonMaterials {
    pub normal: Handle<ColorMaterial>,
    pub hovered: Handle<ColorMaterial>,
    pub pressed: Handle<ColorMaterial>,
}

impl FromWorld for ButtonMaterials {
    fn from_world(world: &mut World) -> Self {
        let mut materials = world.get_resource_mut::<Assets<ColorMaterial>>()
            .expect("Color materials are not available.");
        Self {
            normal: materials.add(Color::DARK_GREEN.into()),
            hovered: materials.add(Color::YELLOW.into()),
            pressed: materials.add(Color::GREEN.into()),
        }
    }
}

pub struct UIPlugin;

impl bevy::app::Plugin for UIPlugin {
    fn build(&self, app: &mut bevy::app::AppBuilder) {
        app
            .init_resource::<ButtonMaterials>()
            .add_startup_system(setup.system())
            .add_system(build_road_button.system());
    }
}

fn setup(
    mut commands: bevy::ecs::system::Commands,
    asset_server: Res<AssetServer>,
    button_materials: Res<ButtonMaterials>
) {
    commands.spawn_bundle(UiCameraBundle::default());
    commands.spawn_bundle(ButtonBundle {
        style: Style {
            size: Size::new(Val::Percent(10f32), Val::Px(30f32)),
            justify_content: JustifyContent::Center,
            align_items: AlignItems::Center,
            ..Default::default()
        },
        material: button_materials.normal.clone(),
        ..Default::default()
    }).with_children(|parent| {
        parent.spawn_bundle(TextBundle {
            text: Text::with_section(
              "Start",
              TextStyle {
                  font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                  font_size: 20.0,
                  color: Color::BLACK,
              },
              Default::default()
            ),
            ..Default::default()
        });
    });
}

fn build_road_button(
    mut commands: bevy::ecs::system::Commands,
    button_materials: Res<ButtonMaterials>,
    road_points: Query<&Transform, With<RoadPoint>>,
    mut road: Query<&mut Handle<Mesh>, With<Road>>,
    mut road_wireframes: Query<
        &mut Line, 
        (With<RoadWireframes>, Without<RoadNormals>, Without<RoadPath>)>,
    mut road_normals: Query<
        &mut Line,
        (With<RoadNormals>, Without<RoadWireframes>, Without<RoadPath>)
    >,
    mut road_path: Query<
        &mut Line,
        (With<RoadPath>, Without<RoadNormals>, Without<RoadWireframes>)
    >,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut interaction_query: Query<
        (&Interaction, &mut Handle<ColorMaterial>),
        (Changed<Interaction>, With<Button>)
    >
) {
    for (interaction, mut material) in interaction_query.iter_mut() {
        match interaction {
            Interaction::Clicked => {
                let width = 25f32;
                let all_points: Vec<_> = road_points.iter()
                    .map(|transform| RoadSection { width, position: transform.translation })
                    .collect();
                //let translation = all_points.first().map_or(Vec3::ZERO, |v| v.position);
                let translation = Vec3::ZERO;
                let sections_line = sections_into_line(&all_points);
                let sections_normals = line_to_normals(&line_to_points(&sections_line));
                let new_road_mesh = generate_road(&all_points);
                create_new_or_replace_road(
                    &mut commands,
                    &mut road,
                    &mut road_wireframes,
                    &mut meshes,
                    &mut materials,
                    new_road_mesh,
                    Color::PURPLE,
                    translation
                );
                create_new_or_replace_path(
                    &mut commands,
                    &mut road_path,
                    sections_line,
                    translation
                );
                create_new_or_replace_normals(
                    &mut commands,
                    &mut road_normals,
                    sections_normals,
                    translation
                );
            }
            Interaction::Hovered => {
                *material = button_materials.hovered.clone();
            }
            Interaction::None => {
                *material = button_materials.normal.clone();
            }
        }
    }
}

fn create_new_or_replace_road(
    commands: &mut bevy::ecs::system::Commands,
    road: &mut Query<&mut Handle<Mesh>, With<Road>>,
    road_wireframes: &mut Query<
        &mut Line,
        (With<RoadWireframes>, Without<RoadNormals>, Without<RoadPath>)
    >,
    meshes: &mut ResMut<Assets<Mesh>>,
    materials: &mut ResMut<Assets<ColorMaterial>>,
    new_mesh: Mesh,
    default_color: Color,
    translation: Vec3,
) {
    match road.single_mut() {
        Ok(mut mesh_handle) => {
            info!("Changing existing sprite!");
            let mut wireframes = road_wireframes.single_mut()
                .expect("Wireframes are not single :(");
            wireframes.points.clear();
            mesh_to_points(&new_mesh, &mut wireframes.points);
            // TODO: Should I remove the handle here? How to deallocate the mesh?
            meshes.remove(mesh_handle.clone());
            *mesh_handle = meshes.add(new_mesh);
        },
        Err(QuerySingleError::NoEntities(e)) => {
            info!("Creating new sprite! {}", e);
            commands.spawn_bundle(LineBundle::from_mesh(&new_mesh))
                .insert(RoadWireframes)
                .insert(Transform {
                    translation,
                    ..Default::default()
                });
            commands.spawn_bundle(SpriteBundle {
                mesh:  meshes.add(new_mesh),
                material: materials.add(default_color.into()),
                sprite: Sprite::new(vec2(1.0, 1.0)),
                transform: Transform {
                    translation,
                    ..Default::default()
                },
                ..Default::default()
            }).insert(Road);
        },
        Err(error) => panic!("Querty must be either empty or with a single element! {}", error),
    };
}

fn create_new_or_replace_path(
    commands: &mut bevy::ecs::system::Commands,
    query: &mut Query<&mut Line, (With<RoadPath>, Without<RoadNormals>, Without<RoadWireframes>)>,
    new_line: Vec<Vec3>,
    translation: Vec3,
) {
    match query.single_mut() {
        Ok(mut line) => {
            info!("Changing existing line!");
            line.points.clear();
            let mut current = *new_line.first().expect("Empty Line");
            for new_point in new_line.iter() {
                line.points.push(Point(current, *new_point));
                current = *new_point;
            }
        },
        Err(QuerySingleError::NoEntities(e)) => {
            info!("Creating new line! {}", e);
            commands.spawn_bundle(LineBundle::from_line(
                new_line,
                Color::BLUE
            ))
            .insert(Transform { translation, ..Default::default() })
            .insert(RoadPath);
        },
        Err(error) => panic!("Querty must be either empty or with a single element! {}", error),
    };
}

fn create_new_or_replace_normals(
    commands: &mut bevy::ecs::system::Commands,
    query: &mut Query<&mut Line, (With<RoadNormals>, Without<RoadWireframes>, Without<RoadPath>)>,
    new_normals: Vec<Point>,
    translation: Vec3,
) {
    match query.single_mut() {
        Ok(mut line) => {
            info!("Changing existing normals!");
            line.points = new_normals;
        },
        Err(QuerySingleError::NoEntities(e)) => {
            info!("Creating new normals! {}", e);
            commands.spawn_bundle(LineBundle::from_points(
                new_normals,
                Color::RED
            ))
            .insert(Transform { translation, ..Default::default() })
            .insert(RoadNormals);
        },
        Err(error) => panic!("Querty must be either empty or with a single element! {}", error),
    };
}
