use crate::math::normal;

use bevy::render::mesh::{Indices, Mesh};
use bevy::math::Vec3;
use bevy::render::pipeline::PrimitiveTopology;

pub struct RoadSection {
    pub width: f32,
    pub position: Vec3
}

pub struct Trajectory {
    positions: Vec<[f32; 3]>,
    normals: Vec<[f32; 3]>,
    uvs: Vec<[f32; 2]>,
}

pub struct Pivot {
    direction: Vec3,
    width: f32
}

pub fn generate_road(sections: &[RoadSection]) -> Mesh {
    let current_pivot = Pivot {
        direction: Vec3::X,
        width: 30f32
    };
    let (_new_pivot, section_trajectory) = build_sections(current_pivot, sections);
    trajectory_to_mesh(vec![section_trajectory])
}

pub fn sections_into_line(sections: &[RoadSection]) -> Vec<Vec3> {
    sections.iter()
        .map(|RoadSection { width: _, position }| *position)
        .collect()
}

pub fn build_sections(pivot: Pivot, sections: &[RoadSection]) -> (Pivot, Trajectory) {
    let mut current_normal = normal(pivot.direction);
    let mut current_direction = pivot.direction;
    let mut current_width = pivot.width;
    let mut trajectory_positions = Vec::new();
    for section in sections.array_windows::<2>() {
        let RoadSection { width: _first_width, position: first_position } = section[0];
        let RoadSection { width: second_width, position: second_position } = section[1];
        let new_direction = second_position - first_position;
        let new_normal = normal(new_direction);
        let first_middle = first_position + new_normal * second_width;
        let first_right = second_position + new_normal * second_width;
        let first_bottom = first_position - new_normal * second_width;
        let next_middle = first_right;
        let next_left = first_bottom;
        let next_bottom = second_position - new_normal * second_width;
        if new_normal != current_normal {
            trajectory_positions.push(first_bottom);
            trajectory_positions.push(first_middle);
            let first_left = first_position + current_normal * current_width;
            trajectory_positions.push(first_left);
            trajectory_positions.push(first_bottom);
            trajectory_positions.push(first_middle);
            let first_left = first_position - current_normal * current_width;
            trajectory_positions.push(first_left);
        }
        trajectory_positions.push(first_middle);
        trajectory_positions.push(first_right);
        trajectory_positions.push(first_bottom);
        trajectory_positions.push(next_bottom);
        trajectory_positions.push(next_left);
        trajectory_positions.push(next_middle);
        current_width = second_width;
        current_normal = new_normal;
        current_direction = new_direction
    }
    let trajectory = Trajectory {
        positions: trajectory_positions.iter().map(|position| [position.x, position.y, position.z]).collect(),
        normals: (0..trajectory_positions.len()).map(|_| [1.0, 1.0, 1.0]).collect(),
        uvs: (0..trajectory_positions.len()).map(|_| [0.0, 0.0]).collect()
    };
    (
        Pivot {
            direction: current_direction,
            width: current_width
        },
        trajectory
    )
}

pub fn trajectory_to_mesh(descriptions: Vec<Trajectory>) -> Mesh {
    let mut positions = Vec::new();
    let mut normals = Vec::new();
    let mut uvs = Vec::new();
    for description in descriptions.into_iter() {
        positions.extend(description.positions);
        normals.extend(description.normals);
        uvs.extend(description.uvs);
    }
    let indices = (0..positions.len()).map(|value| value as u32).collect();
    let mut mesh = Mesh::new(PrimitiveTopology::TriangleList);
    mesh.set_attribute(Mesh::ATTRIBUTE_NORMAL, normals);
    mesh.set_attribute(Mesh::ATTRIBUTE_UV_0, uvs);
    mesh.set_indices(Some(Indices::U32(indices)));
    mesh.set_attribute(Mesh::ATTRIBUTE_POSITION, positions);
    mesh
}

pub fn copy_mesh(source: Mesh, destination: &mut Mesh) {
    destination.set_attribute(Mesh::ATTRIBUTE_NORMAL, source.attribute(Mesh::ATTRIBUTE_NORMAL).unwrap().clone());
    destination.set_attribute(Mesh::ATTRIBUTE_UV_0, source.attribute(Mesh::ATTRIBUTE_UV_0).unwrap().clone());
    destination.set_attribute(Mesh::ATTRIBUTE_POSITION, source.attribute(Mesh::ATTRIBUTE_POSITION).unwrap().clone());
    destination.set_indices(Some(source.indices().unwrap().clone()));
}
