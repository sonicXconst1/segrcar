#![feature(array_windows)]
pub mod resources;
pub mod road;
pub mod line;
pub mod cursor;
pub mod collision; 
pub mod road_builder;
pub mod ui;
pub mod physics;
pub mod math;
use collision::Collider;
use bevy::app::EventReader;
use bevy::input::mouse::MouseWheel;
use line::{LineBundle, create_line};
use bevy::render::camera::Camera;
use bevy::window::Windows;
use bevy::prelude::shape;
use bevy::ecs::query::{With, Without};
use bevy::ecs::system::{
    IntoSystem,
    Query,
    Commands,
    Res,
    ResMut
};
use bevy::input::{Input, keyboard::KeyCode};
use bevy::sprite::ColorMaterial;
use bevy::render::color::Color;
use bevy::render::mesh::Mesh;
use bevy::asset::Assets;
use bevy::core::Time;
use bevy::math::{Vec2, Vec3, Quat, vec2, vec3};
use bevy::transform::components::Transform;
use bevy::render::entity::OrthographicCameraBundle;
use bevy::sprite::{Sprite, entity::SpriteBundle};
use bevy::ecs::bundle::Bundle;

#[derive(Bundle)]
struct HeroBundle {
    name: Name,
    health: Health,
    position: Vec3,
}

#[derive(Default)]
pub struct Car {
    pub crashed: bool,
    pub velocity: Vec3
}

struct Health(usize);

struct Name(String);

struct Wall;

struct MainCamera;

struct Road;
pub struct RoadPath;
pub struct RoadWireframes;
pub struct RoadNormals;

fn main() {
    bevy::app::App::build()
        .add_plugins(bevy::DefaultPlugins)
        .add_plugin(line::LinePlugin)
        .add_plugin(cursor::CursorPlugin)
        .add_plugin(collision::DrawCollidersPlugin)
        .add_plugin(road_builder::RoadBuilderPlugin)
        .add_plugin(ui::UIPlugin)
        //.add_plugin(bevy::diagnostic::FrameTimeDiagnosticsPlugin::default())
        //.add_plugin(bevy::diagnostic::LogDiagnosticsPlugin::default())
        .init_resource::<resources::GameResources>()
        .add_startup_system(startup.system())
        .add_system(keyboard_control.system())
        .add_system(car_collision_system.system())
        .add_system(camera_move_system.system())
        .run()
}

fn keyboard_control(
    input: Res<Input<KeyCode>>,
    window: Res<Windows>,
    time: Res<Time>,
    mut cars: Query<(&Car, &mut Transform)>,
) {
    if input.pressed(KeyCode::R) {
        let shift_step = 30f32;
        let mut shift = Vec3::ZERO;
        for (_car, mut transform) in cars.iter_mut() {
            transform.translation = shift;
            transform.rotation = Quat::IDENTITY;
            shift.y += shift_step;
        }
        return;
    }
    let primary_window = window.get_primary().unwrap();
    let height = primary_window.height() / 5f32;
    let vertical_speed = height * time.delta_seconds(); 
    let mut position_shift = 0f32;
    let mut angle = 0f32;
    if input.pressed(KeyCode::Up) {
        position_shift += vertical_speed;
    }
    if input.pressed(KeyCode::Down) {
        position_shift -= vertical_speed;
    }
    if input.pressed(KeyCode::Right) {
        angle -= 1f32;
    }
    if input.pressed(KeyCode::Left) {
        angle += 1f32;
    }
    for (car, mut transform) in cars.iter_mut() {
        if car.crashed {
            transform.translation = Vec3::ZERO;
            transform.rotation = Quat::IDENTITY;
        }
        else {
            let (axis, mut current_angle) = transform.rotation.to_axis_angle();
            current_angle = 2f32 * std::f32::consts::PI + current_angle * axis.z;
            transform.translation += Vec3::new(
                position_shift * (current_angle.cos()),
                position_shift * current_angle.sin(),
                0f32);
            transform.rotation *= Quat::from_rotation_z(angle / 20f32); 
        }
    }
}

fn camera_move_system(
    mut commands: Commands,
    mut camera: Query<&mut Transform, (With<MainCamera>, With<Camera>)>,
    cursor_state: Query<&cursor::CursorState>,
    keys: Res<Input<KeyCode>>,
    mut wheel_events: EventReader<MouseWheel>,
    mut dragging: EventReader<cursor::Dragging>
) {
    let mut transform = camera.single_mut().expect("Camera is missing");
    for event in wheel_events.iter() {
        transform.rotation *= Quat::from_rotation_x(event.y / 20f32);
    }
    if keys.pressed(KeyCode::S) {
        transform.translation.z += 1f32;
    }
    if keys.pressed(KeyCode::W) {
        transform.translation.z -= 1f32;
    }
    let _cursor_state = cursor_state.single().expect("Only 1 cursor expected..");
    for dragging_event in dragging.iter() {
        let mut shift = -dragging_event.shift_local;
        shift.z = 0f32;
        commands.spawn_bundle(LineBundle::from_points(
            vec![
                line::Point(
                    vec3(transform.translation.x, transform.translation.y, 0f32),
                    vec3(transform.translation.x + shift.x, transform.translation.y + shift.y, 0f32)
                )
            ], 
            Color::BLUE));
        transform.translation += shift;
    }
}

fn car_collision_system(
    mut cars: Query<(&mut Car, &Transform)>,
    colliders: Query<(&Collider, &Transform), Without<Car>>,
) {
    let car_size = Vec2::new(50f32, 30f32);
    for (mut car, car_transform) in cars.iter_mut() {
        let mut collision_happened = false;
        for (collider, collider_transform) in colliders.iter() {
            let collision = match collider {
                Collider::Quad(size) => collision::quad_collision(
                    &car_transform,
                    car_size,
                    &collider_transform,
                    size.clone())
            };
            if collision {
                collision_happened = true;
                break;
            } 
        }
        car.crashed = collision_happened;
    }
}

fn startup(
    mut commands: Commands,
    windows: Res<Windows>,
    game_resource: Res<resources::GameResources>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut meshes: ResMut<Assets<Mesh>>,
) {
    let window = windows.get_primary().unwrap();
    let width = window.width();

    commands.spawn_bundle(OrthographicCameraBundle::new_2d())
        .insert(MainCamera);
    commands.spawn_bundle(SpriteBundle {
        material: materials.add(ColorMaterial::color(Color::rgb(1.0, 0.1, 0.5))),
        sprite: Sprite::new(vec2(1.0, 1.0)),
        mesh: meshes.add(create_line(vec![
            [vec3(-100f32, 000f32, 0f32), vec3(-200f32, 100f32, 0f32)],
            [vec3(-200f32, 100f32, 0f32), vec3(-200f32, -100f32, 0f32)],
            [vec3(-200f32, -100f32, 0f32), vec3(-100f32, 000f32, 0f32)],
        ])),
        ..Default::default()
    });

    commands.spawn_bundle(SpriteBundle {
        mesh: meshes.add(shape::Cube { size: 10f32 }.into()),
        material: materials.add(ColorMaterial::color(Color::rgb(1.0, 1.0, 0.0))),
        sprite: Sprite::new(vec2(1f32, 1f32)),
        transform: Transform {
            translation: Vec3::new(-100.0, 0.0, 0.0),
            ..Default::default()
        },
        ..Default::default()
    });
    commands
        .spawn_bundle(SpriteBundle {
            material: game_resource.car.clone(),
            transform: Transform {
                translation: Vec3::new(0.0, 0.0, 0.0),
                scale: Vec3::splat(game_resource.car_scale),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(Car::default())
        .insert(Collider::Quad(vec2(50f32, 30f32)));

    commands.spawn_bundle(SpriteBundle {
        material: materials.add(Color::rgb(1.0, 1.0, 0.2).into()),
        sprite: Sprite {
            size: Vec2::new(width / 4f32, 10f32),
            ..Default::default()
        },
        transform: Transform {
            translation: Vec3::new(0f32, 100f32, 0f32),
            ..Default::default()
        },
        ..Default::default()
    }).insert(Collider::Quad(vec2(width / 4f32, 10f32))).insert(Wall);
}
