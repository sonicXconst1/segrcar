use bevy::math::Vec3;

pub fn normal(vec: Vec3) -> Vec3 {
    // TODO: bug expected if direction is close to 0.
    vec.cross(Vec3::Z).normalize()
}
