use crate::line::{LineBundle, Line, Point};
use crate::cursor::CursorState;

use bevy::log::{info, error};
use bevy::prelude::{DespawnRecursiveExt, ParallelSystemDescriptorCoercion, shape};
use bevy::asset::{Assets, Handle};
use bevy::ecs::system::{IntoSystem, Res, ResMut, Query};
use bevy::ecs::schedule::SystemLabel;
use bevy::ecs::entity::Entity;
use bevy::transform::components::Transform;
use bevy::ecs::query::With;
use bevy::math::{Vec3, vec2};
use bevy::input::{Input, mouse::MouseButton, keyboard::KeyCode};
use bevy::render::color::Color;
use bevy::render::mesh::Mesh;
use bevy::sprite::{Sprite, ColorMaterial, entity::SpriteBundle};

#[derive(Debug, Clone, Eq, PartialEq, Hash, SystemLabel)]
enum RoadBuilderSystem {
    AddBuildingPoint,
    HoveringBuildingPoint,
    RemoveBuildingPoint,
    DrawBuildingPoints
}

pub struct RoadBuilderPlugin;

pub struct RoadLine;

pub struct RoadPoint {
    pub position: Vec3
}

#[derive(Default)]
pub struct RoadBuildingState {
    points: Vec<Vec3>,
    entities: Vec<u32>
}

pub struct RoadPointMaterials {
    pub hovering: Handle<ColorMaterial>,
    pub normal: Handle<ColorMaterial>
}

pub struct Hovering;

impl bevy::app::Plugin for RoadBuilderPlugin {
    fn build(&self, app: &mut bevy::app::AppBuilder) {
        app
            .init_resource::<RoadPointMaterials>()
            .init_resource::<RoadBuildingState>()
            .add_startup_system(setup.system())
            .add_system(
                add_building_point.system()
                .label(RoadBuilderSystem::AddBuildingPoint)
            )
            .add_system(
                hovering_building_point.system()
                .label(RoadBuilderSystem::HoveringBuildingPoint)
                .after(RoadBuilderSystem::AddBuildingPoint)
            )
            .add_system(
                remove_building_point.system()
                .label(RoadBuilderSystem::RemoveBuildingPoint)
                .after(RoadBuilderSystem::HoveringBuildingPoint)
            )
            .add_system(
                draw_road.system()
                .label(RoadBuilderSystem::DrawBuildingPoints)
                .after(RoadBuilderSystem::HoveringBuildingPoint)
            );
    }
}

impl bevy::ecs::world::FromWorld for RoadPointMaterials {
    fn from_world(world: &mut bevy::ecs::world::World) -> Self {
        let mut materials = world.get_resource_mut::<Assets<ColorMaterial>>()
            .expect("Failed to find materials.");
        Self {
            normal: materials.add(Color::GREEN.into()),
            hovering: materials.add(Color::BLUE.into()),
        }
    }
}

fn setup(mut commands: bevy::ecs::system::Commands) {
    commands.spawn_bundle(LineBundle::from_points(vec![], Color::ORANGE))
        .insert(RoadLine);
}

fn add_building_point(
    mut commands: bevy::ecs::system::Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut road_state: ResMut<RoadBuildingState>,
    point_materials: Res<RoadPointMaterials>,
    mouse_input: Res<Input<MouseButton>>,
    keyboard_input: Res<Input<KeyCode>>,
    cursor: Query<&CursorState>,
) {
    let cursor = cursor.single().expect("More the one cursor!");
    if keyboard_input.pressed(KeyCode::LControl) && mouse_input.just_released(MouseButton::Left) {
        let mut position = cursor.position_world;
        position.z = 0f32;
        let id = commands.spawn_bundle(SpriteBundle {
            mesh: meshes.add(shape::Quad { size: vec2(10f32, 10f32), flip: false }.into()),
            sprite: Sprite::new(vec2(1.0, 1.0)),
            material: point_materials.normal.clone(),
            transform: Transform {
                translation: position,
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(RoadPoint { position }).id();
        road_state.points.push(position);
        road_state.entities.push(id.id());
    }
}

fn hovering_building_point(
    mut commands: bevy::ecs::system::Commands,
    road_point_materials: Res<RoadPointMaterials>,
    cursor: Query<&CursorState>,
    mut building_points: Query<(Entity, &mut Handle<ColorMaterial>, &Transform), With<RoadPoint>>
) {
    let cursor = cursor.single().expect("Cursor is not single.");
    let cursor_position = cursor.position_world;
    let radius = 10f32;
    for (entity, mut point_material, transform) in building_points.iter_mut() {
        if (transform.translation - cursor_position).length() < radius {
            *point_material = road_point_materials.hovering.clone();
            commands.entity(entity).insert(Hovering);
        } else {
            *point_material = road_point_materials.normal.clone();
            commands.entity(entity).remove::<Hovering>();
        }
    }
}

fn remove_building_point(
    mut commands: bevy::ecs::system::Commands,
    mut road_state: ResMut<RoadBuildingState>,
    mouse_input: Res<Input<MouseButton>>,
    building_points: Query<(Entity, &RoadPoint), With<Hovering>>,
) {
    if mouse_input.just_released(MouseButton::Right) {
        for (entity, _point) in building_points.iter() {
            info!("Remove building point with id: {}", entity.id());
            let index = road_state.entities.iter()
                .enumerate()
                .find(|(_index, id)| **id == entity.id())
                .map_or(None, |(index, _id)| Some(index));
            if let Some(index) = index {
                info!("Entity and point index is: {}", index);
                road_state.points.remove(index);
                road_state.entities.remove(index);
                commands.entity(entity).despawn_recursive();
            } else {
                error!("Failed to find entity with id {}", entity.id());
            }
        }
    }
}

fn draw_road(
    road_state: Res<RoadBuildingState>,
    mut road_line: Query<&mut Line, With<RoadLine>>
) {
    let mut road_line = road_line.single_mut().expect("Only one road is possible.");
    road_line.points.clear();
    if road_state.points.len() > 1 {
        for points in road_state.points.as_slice().array_windows::<2>() {
            road_line.points.push(Point(points[0], points[1]));
        }
    }
}
