use bevy::app::{EventReader, EventWriter, Plugin, AppBuilder};
use bevy::input::{Input, mouse::MouseButton};
use bevy::math::{Vec3, vec2};
use bevy::window::{CursorMoved, Windows};
use bevy::ecs::system::{Query, ResMut, Res, Commands, IntoSystem};
use bevy::transform::components::Transform;
use bevy::render::camera::Camera;
use bevy::ecs::query::With;
use bevy::ecs::schedule::SystemLabel;
use bevy::prelude::ParallelSystemDescriptorCoercion;

#[derive(Debug, Clone, Eq, PartialEq, Hash, SystemLabel)]
pub enum CursorPluginSystem {
    Setup,
    CursorState,
}

#[derive(Default)]
pub struct Dragged {
    pub from_local: Vec3,
    pub from_world: Vec3,
    pub to_local: Vec3,
    pub to_world: Vec3,
}

#[derive(Default)]
pub struct Dragging {
    pub shift_local: Vec3,
    pub shift_world: Vec3,
}

impl Dragged {
    pub fn shift_world(&self) -> Vec3 {
        self.to_world - self.from_world
    }
}

#[derive(Default)]
pub struct DraggingState {
    pub from_local: Vec3,
    pub from_world: Vec3,
    pub to_local: Vec3,
    pub to_world: Vec3,
}

pub struct CursorPlugin;

#[derive(Default)]
pub struct CursorState {
    pub position_world: Vec3,
    pub position_local: Vec3,
}

impl Plugin for CursorPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app
            .init_resource::<DraggingState>()
            .add_event::<Dragging>()
            .add_event::<Dragged>()
            .add_startup_system(
                setup.system().label(CursorPluginSystem::Setup))
            .add_system(cursor_state.system().label(CursorPluginSystem::CursorState));
    }
}

fn setup(
    mut commands: Commands
) {
    commands.spawn()
        .insert(CursorState::default());
}

fn local_to_world(pivot: &Transform, v: Vec3) -> Vec3 {
    let local_x = pivot.local_x();
    let local_y = pivot.local_y();
    let local_z = pivot.local_z();
    pivot.translation + local_x * v.x + local_y * v.y + local_z * v.z
}


fn cursor_state(
    windows: Res<Windows>,
    mut events: EventReader<CursorMoved>,
    mouse_input: Res<Input<MouseButton>>,
    camera: Query<&Transform, (With<crate::MainCamera>, With<Camera>)>,
    mut cursor_state: Query<&mut CursorState>,
    mut dragging_state: ResMut<DraggingState>,
    mut dragging: EventWriter<Dragging>,
    mut dragged: EventWriter<Dragged>
) {
    let window = windows.get_primary().expect("Failed to get window.");
    let mut cursor_state = cursor_state.single_mut()
        .expect("More then 1 cursor");
    let camera_transform = camera.single().expect("No camera found");
    for event in events.iter() {
        let window_size = vec2(window.width(), window.height());
        let mouse_position_local = event.position - window_size / 2f32;
        let mouse_position_world = local_to_world(
            camera_transform,
            mouse_position_local.extend(-camera_transform.translation.z));
        cursor_state.position_world = mouse_position_world;
        cursor_state.position_local = mouse_position_local.extend(0f32);
        //info!("Mouse local position {:?}", mouse_position_local);
        //info!("Mouse world position {:?}", mouse_position_world);
    }
    if mouse_input.just_pressed(MouseButton::Left) {
        dragging_state.from_local = cursor_state.position_local;
        dragging_state.from_world = cursor_state.position_world;
        dragging_state.to_local = cursor_state.position_local;
        dragging_state.to_world = cursor_state.position_world;
    }
    if mouse_input.pressed(MouseButton::Left) {
        write_dragging(
            &mut dragging,
            &mut dragging_state,
            cursor_state.position_local,
            cursor_state.position_world);
    }
    if mouse_input.just_released(MouseButton::Left) {
        write_dragging(
            &mut dragging,
            &mut dragging_state,
            cursor_state.position_local,
            cursor_state.position_world);
        write_dragged(&mut dragged, &dragging_state);
    }
}

fn write_dragging(
    dragging: &mut EventWriter<Dragging>,
    dragging_state: &mut ResMut<DraggingState>,
    to_local: Vec3,
    to_world: Vec3
) {
    let dragging_event = Dragging {
        shift_local: to_local - dragging_state.to_local,
        shift_world: to_world - dragging_state.to_world,
    };
    dragging_state.to_local = to_local;
    dragging_state.to_world = to_world;
    dragging.send(dragging_event)
}

fn write_dragged(
        writer: &mut EventWriter<Dragged>,
        dragging: &DraggingState) {
    let dragged = Dragged {
        from_local: dragging.from_local,
        from_world: dragging.from_world,
        to_local: dragging.to_local,
        to_world: dragging.to_world
    };
    writer.send(dragged)
}

mod test {
    #[test]
    fn test_local_to_world() {
        let pivot = super::Transform {
            translation: super::Vec3::ONE,
            ..Default::default()
        };
        let local = bevy::math::vec3(1f32, 1f32, 1f32);
        let result = super::local_to_world(&pivot, local);
        assert_eq!(result, pivot.translation + local, "Error"); 
    }
}
