use bevy::math::Vec3;

pub fn distance(velocity: Vec3, time: f32) -> Vec3 {
    velocity * time
}

pub fn position(start: Vec3, velocity: Vec3, time: f32) -> Vec3 {
    start + distance(velocity, time)
}

pub fn turn(velocity: Vec3, new_direction: Vec3) -> Vec3 {
    let direction = velocity.normalize() + new_direction.normalize();
    direction.normalize() * velocity.length()
}
